<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('plantilla'); 
});


/*

//######SECCCIÓN PARA DAR DE ALTA LAS RUTAS DE NUESTRAS VISTAS######//
//Route::view('/', 'paginas.inicio'); //para la vista que tome lo que esta en la carpeta paginas en el archivo blog. 
//Route::view('/home' , 'home');

Route::view('/variacionvue', 'paginas.variacionvue');


Route::view('/','paginas.blog');
Route::view('/administradores', 'paginas.administradores');
Route::view('/categorias', 'paginas.categorias');
Route::view('/articulos','paginas.articulos');
Route::view('/opiniones', 'paginas.opiniones');
Route::view('/banner','paginas.banner');
Route::view('/anuncios', 'paginas.anuncios');
Route::view('/modular','paginas.modular');
Route::view('/jerarquica','paginas.jerarquica');
Route::view('/columnas','paginas.columnas');   
Route::view('/variacion','paginas.variacion');
Route::view('/grid', 'paginas.grid'); 
Route::view('/grid2','paginas.vue-grid-layout-master.examples.01-basic'); //ruta de la reticula variacion violacion
//Route::view('/grid3', 'paginas.vue-grd-master.index'); //ruta de la reticula modular primer reticula totalmente editable
//Route::view('/grid3', 'paginas.vue-grd-master.index'); //ruta de la reticula modular primer reticula totalmente editable
//
 
Route::get('/','BlogController@traerBlog');   //nombre del controlador y metodo  
Route::get('/administradores','AdministradoresController@traerAdministradores');
Route::get('/categorias', 'CategoriasController@traerCategorias');
Route::get('/articulos', 'ArticulosController@traerArticulos');
Route::get('/opiniones', 'OpinionesController@traerOpiniones');
Route::get('/banner', 'BannerController@traerBanners');
Route::get('/anuncios', 'AnunciosController@traerAnuncios'); 
*/

Route::view('/grid2','paginas.vue-grid-layout-master.examples.01-basic'); //ruta de la reticula variacion violacion

/*funcion anonima*/
Route::get('/grid3', function(){
	//header('Location:http://localhost/vue-grd-master/');
	//return "hola como estas!";
	return redirect('http://localhost/vue-grd-master/');
}); 


/*funcion anonima*/
/*
Route::get('/grid2', function(){
	//header('Location:http://localhost/vue-grd-master/');
	//return "hola como estas!";
	return redirect('http://localhost/vue-grid-layout-master/examples/01-basic');
});
*/ 
/*
Route::get('/grid2', function(){

	return redirect('paginas/vue-grid-layout-master/examples/01-basic.html');
});
*/




//redirigir al index.pnp para que la pagina recargue 
	//header('location:index.php'); 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//RUTAS QUE INCLUYEN TODOS LOS METODOS HTTP 
//RECURSOS DE RUTA Route:resouce()
//php artisan reoute:list

//agregamos todos los metodos que vamos a utilizar del controlador Blog
////aqui estamos agregando todos los metodos http que vamos a utilizar de el controlador Blog 
Route::resource('/','BlogController'); //laravel no permite trabajar los metodos store .show etc.. en la ruta principal por eso abajo le creo otra ruta y repito el controlador
Route::resource('/blog','BlogController'); 
Route::resource('/administradores','AdministradoresController');
Route::resource('/categorias', 'CategoriasController');
Route::resource('/articulos', 'ArticulosController');
Route::resource('/opiniones', 'OpinionesController');
Route::resource('/banner', 'BannerController');
Route::resource('/anuncios', 'AnunciosController'); 
