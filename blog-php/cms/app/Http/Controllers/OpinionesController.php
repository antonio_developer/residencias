<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OpinionesModel;

class OpinionesController extends Controller
{
    public function index(){

    	$opiniones = OpinionesModel::all(); 

    	return view("paginas.opiniones", array("opiniones"=>$opiniones));
    }
}
