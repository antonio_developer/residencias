<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BlogModel; //de este modo me traigo la clase BlogModel del modelo (el archivo BlogModel que esta en la carpeta app donde se guardan los modelos) 

class BlogController extends Controller
{
	/*=============================================
	Mostrar todos los registros
	=============================================*/	
     public  function index(){
    	$blog = BlogModel::all(); // fetchall() traer todo lo de la tabla 
    	
    	return view("paginas.blog", array("blog"=>$blog)); //aqui ya le estoy enviando a la vista todo lo que estoy alamacenando en el objeto Blog que es todo lo que tiene la tabla 
    }

    /*=============================================
    Actualizar un registro
    =============================================*/
    public function update($id, Request $request){ //el update recibe el id y los requerimientos (estos se guardan en un objeto)
    	//recoger los datos 
    	$datos = array("dominio"=>$request->input("dominio"),
    					"servidor"=>$request->input("servidor"),
    					"titulo"=>$request->input("titulo"),
    					"descripcion"=>$request->input("descripcion"),); 

    	echo '<pre>'; print_r($datos); echo '</pre>'; 

    	return; 
    }
    
    
    
    

    public function store(){

    }
}
