<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\BannerModel; 

class BannerController extends Controller
{
    public function index(){

    	$banner = BannerModel::all(); //fetchall de php 

    	return view("paginas.banner", array("banner"=>$banner));
    }
}
