<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ArticulosModel; 

class ArticulosController extends Controller
{
	public function index(){

		$articulos = ArticulosModel::all(); 

		return view("paginas.articulos",array("articulos"=>$articulos));
	}
    
}
