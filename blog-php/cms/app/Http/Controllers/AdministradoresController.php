<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AdministradoresModel; 

class AdministradoresController extends Controller
{
    public function index(){

    	$administradores  = AdministradoresModel::all(); 

    	return view("paginas.administradores", array("administradores"=>$administradores)); 
    }
}
