<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AnunciosModel; 

class AnunciosController extends Controller
{
	public function index(){

		$anuncios = AnunciosModel::all(); 

		return view("paginas.anuncios", array("anuncios"=>$anuncios)); 
	}   
}
