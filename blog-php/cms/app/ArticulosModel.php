<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticulosModel extends Model
{
    protected $table = 'articulos'; 

    /*==================================================
    =            INNER JOIN DESDE EL MODELO            =
    ==================================================*/
   	public function categorias(){

   		return $this->belongsTo('App\CategoriasModel', 'id_cat', 'id_categoria'); 
   	}
    
    
    
    
}
