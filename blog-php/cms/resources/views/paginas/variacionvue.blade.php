
@extends('plantilla') {{--hacemos mension a la plantilla principal que es donde esta el @yield()--}}

@section('content'){{--encapsulamos toda esta vista para convertir su contenido en dinamico y pueda ser embebida por el @yield que esta en nuestra plantilla --}}
@extends('layouts.app')


<div class="content-wrapper" style="min-height: 184px;">

  <!-- Content Header (Page header) -->
  <section class="content-header">

    <div class="container-fluid">

      <div class="row mb-2">

        <div class="col-sm-6">

          <h1>Creación de Retículas de varuacion con vue</h1>
        </div>
        <div class="col-sm-6">

          <ol class="breadcrumb float-sm-right">

            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>

            <!--<li class="breadcrumb-item"><a href="#">Layout</a></li>-->

            <li class="breadcrumb-item active">Blog</li>

          </ol>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="container-fluid">

      <div class="row">

        <div class="col-12">

          <!-- Default box -->
          <div class="card">

            <div class="card-header">

              <h3 class="card-title">Title</h3>

              <div class="card-tools">

                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">

                    <i class="fas fa-minus"></i></button>

                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>

              </div>

            </div>

            <div class="card-body">
              <!--este es el body principal para incrustar la info--> 

              
              variacion vue aqui esta es

              <!--aqui voy a meter el componente--> 
              <div id="app">
                <reticula-variacion-componente></reticula-variacion-componente>
              </div>
              

              

               
            

            </div>

            <!-- /.card-body -->
            <div class="card-footer">
              Footer
              
            </div>

            <!-- /.card-footer-->
          </div>

          <!-- /.card -->
        </div>

      </div>

    </div>

    <!--<script src="vue.min.js"></script>-->
    <script type="../../views/paginas/vue-grid-layout-master/examples/vue.min.js"></script>

    <script src="../dist/vue-grid-layout.umd.min.js"></script>


    <!--<script src="01-basic.js"></script>-->
   
    <script src="01-basic.js"></script>

  </section>

  <!-- /.content -->
</div>

@endsection
{{--Siempre se debe cerrar la etiqueta de @section con @endsection--}}