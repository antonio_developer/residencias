@extends('plantilla') {{--hacemos mension a la plantilla principal que es donde esta el @yield()--}}

@section('content'){{--encapsulamos toda esta vista para convertir su contenido en dinamico y pueda ser embebida por el @yield que esta en nuestra plantilla --}}

<div class="content-wrapper" style="min-height: 184px;">

  <!-- Content Header (Page header) -->
  <section class="content-header">

    <div class="container-fluid">

      <div class="row mb-2">

        <div class="col-sm-6">

          <h1>Creación de Retículas de Variación y Violación</h1>
        </div>
        <div class="col-sm-6">

          <ol class="breadcrumb float-sm-right">

            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>

            <!--<li class="breadcrumb-item"><a href="#">Layout</a></li>-->

            <li class="breadcrumb-item active">Blog</li>

          </ol>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="container-fluid">

      <div class="row">

        <div class="col-12">

          <!-- Default box -->
          <div class="card">

            <div class="card-header">

              <h3 class="card-title">Title</h3>

              <div class="card-tools">

                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">

                    <i class="fas fa-minus"></i></button>

                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>

              </div>

            </div>

            <div class="card-body">
              variacion y violacion
            <!------------------------------------------------------------------->


            
<body>
    <h1>Retícula Modular</h1>

  
   

    <div id="app" style="width: 100%;">
        
        <!--<pre>{{-- $data | json --}}</pre>-->

        <button @click="increaseWidth">Incrementar Ancho</button>
        <button @click=" decreaseWidth">Decrementar Ancho</button>
        <button @click="addItem">Agregar Módulo</button>
        <button @click="removeItem">Borrar Módulo</button>



        <div>
            <div class="layoutJSON">
                Displayed as <code>[x, y, w, h]</code>:
                <div class="columns">
                    <div class="layoutItem" v-for="item in layout">
                        <b>{{item.i}}</b>: [{{item.x}}, {{item.y}}, {{item.w}}, {{item.h}}]
                    </div>
                </div>
            </div>
        </div>
        <div id="content">
            <!--<button @click="decreaseWidth">Decrease Width</button>
            <button @click="increaseWidth">Increase Width</button>
            <button @click="addItem">Add an item</button>-->
            <input type="checkbox" v-model="draggable"/> Draggable
            <input type="checkbox" v-model="resizable"/> Resizable
            <br/>
            <grid-layout :layout="layout"
                         :col-num="12"
                         :row-height="30"
                         :is-draggable="draggable"
                         :is-resizable="resizable"
                         :vertical-compact="true"
                         :use-css-transforms="true"
            >
                <grid-item v-for="item in layout"
                           :static="item.static"
                           :x="item.x"
                           :y="item.y"
                           :w="item.w"
                           :h="item.h"
                           :i="item.i"
                        >
                    <span class="text">{{itemTitle(item)}}</span>
                </grid-item>
            </grid-layout>
        </div>

    </div>
    <script src="vue.min.js"></script>
    <!--
    <script src="../dist/vue-grid-layout.umd.min.js"></script>
    <script src="01-basic.js"></script>
  -->

    <script src="../dist/vue-grid-layout.umd.min.js"></script>
    <script src="01-basic.js"></script>
</body>

     

            <!--------------------------------------------------------------->


            </div>

            <!-- /.card-body -->
            <div class="card-footer">
              Footer
            </div>

            <!-- /.card-footer-->
          </div>

          <!-- /.card -->
        </div>

      </div>

    </div>

  </section>

  <!-- /.content -->
</div>

@endsection
{{--Siempre se debe cerrar la etiqueta de @section con @endsection--}}