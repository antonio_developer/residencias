@extends('plantilla') {{--hacemos mension a la plantilla principal que es donde esta el @yield()--}}

@section('content'){{--encapsulamos toda esta vista para convertir su contenido en dinamico y pueda ser embebida por el @yield que esta en nuestra plantilla --}}

<div class="content-wrapper" style="min-height: 184px;">

  <!-- Content Header (Page header) -->
  <section class="content-header">

    <div class="container-fluid">

      <div class="row mb-2">

        <div class="col-sm-6">

          <h1>Inicio</h1>
        </div>
        <div class="col-sm-6">

          <ol class="breadcrumb float-sm-right">

            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>

            <!--<li class="breadcrumb-item"><a href="#">Layout</a></li>-->

            <li class="breadcrumb-item active">Blog</li>

          </ol>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="container-fluid">

      <div class="row">

        <div class="col-12">

          @foreach ($blog as $element)
             {{--este foreach debe quedar por fuera del form para poder usar en el form el objeto $element--}}
          @endforeach

          <form action="{{url('/')}}/blog/{{  $element->id  }}" method="post">

            @method('PUT') <!-- esta linea genera esto: <input type="hidden" name="_method" value="PUT" == $0-->
            
            @csrf {{--evitar ataque : cross site request forgery creando token aleatorio para saver si el usuario puede hacer un proceso HTTP PUT GET DELET ETC...--}}

            {{--Un ataque CSRF fuerza al navegador web validado de una victima a enviar una petición a una aplicación web vulnerable, la cual entonces realiza la acción elegida a través de la víctima. Al contrario que en los ataques XSS, los cuales explotan la confianza que un usuario tiene en un sitio en particular, el cross site request forgery explota la confianza que un tiene un en un usuario en particular--}}
            
          <!-- Default box -->
          <div class="card"> <!--este card es el principal / encierra todas las entradas que he creado-->

            <div class="card-header">

              <button type="submit" class="btn btn-primary float-right">Guardar Cambios</button>

              <h3 class="card-title">Modificar Entrada</h3>

              <div class="card-tools">


              </div>

            </div>

            <div class="card-body">

              <h3>
              esta es la vista blog.blade
              </h3>
              <br>
                {{----}}
              
              
              <div class="row">

                <div class="col-lg-7">
                  <div class="card">
                    <div class="card-body">
                      
                      {{-- Dominio --}}
                      <div class="input-group mb-3">
                        
                        <div class="input-group-append">
                          
                          <span class="input-group-text">Dominio</span>

                        </div>    

                        <input type="text" class="form-control" name="dominio" value="{{$element->dominio}}" required>
                      
                      </div>

                      {{-- Servidor --}}
                      <div class="input-group mb-3">
                        
                        <div class="input-group-append">
                          
                          <span class="input-group-text">Servidor</span>

                        </div>    

                        <input type="text" class="form-control" name="dominio" value="{{$element->servidor}}" required>
                      
                      </div>

                      {{-- Titulo --}}
                      <div class="input-group mb-3">
                        
                        <div class="input-group-append">
                          
                          <span class="input-group-text">Título</span>

                        </div>    

                        <input type="text" class="form-control" name="dominio" value="{{$element->titulo
                        }}" required>
                      
                      </div>

                      {{-- Descripción --}}
                      <div class="input-group mb-3">
                        
                        <div class="input-group-append">
                          
                          <span class="input-group-text">Descripción</span>

                        </div>    

                        <textarea class="form-control" rows="5" name="descripcion" required>{{$element->descripcion}}</textarea>
                      
                      </div>

                      <hr class="pb-2">

                      {{-- Palabras Claves --}}
                      <div class="form-group mb-3">
                        
                        <label>Palabras Claves</label>
                        @php
                          $tags = json_decode($element->palabras_claves, true); //convierto el string de la bd en array, asi ya puedo tomar cada uno de sus indices 0,1,2 etc.....
                          //echo '<pre>'; print_r($tags); echo '</pre>';
                          
                          $palabras_claves = ""; 

                          foreach ($tags as $key => $value){
                            $palabras_claves .= $value . ","; 
                          }
                          
                        @endphp

                        {{--agregando el plugin  tagsinput para las palabras clave solo es dar de alta los archivos css y js en la plantilla.blade.php y agregar data-role aqui abajo--}}
                        <input type="text" class="form-control" name="palabras_claves" value="{{$palabras_claves}}" data-role="tagsinput" required>

                      </div>

                      <hr class="pb-2">

                      {{--Redes Sociales--}}
                      <label>Redes Sociales</label>
                      <div class="row">
                        <div class="col-5">
                          <div class="input-group mb-3">
                            
                            <div class="input-group-append">
                              <span class="input-group-text">Icono</span>
                            </div>
                            
                            <select class="form-control" id="icono_red">
                              {{--todas las redes sociales que permitiremos al usuario Del CMS--}}

                              <option value="fab fa-facebook-f, #1475E0">
                                facebook
                              </option>

                              <option value="fab fa-instagram, #B18768">
                                Instagram
                              </option>

                              <option value="fab fa-twitter, #00A6FF">
                                Twitter
                              </option>

                               <option value="fab fa-youtube, #F95F62">
                                Youtube
                              </option>

                              <option value="fab fa-snapchat, #FF9052">
                                Snapchat
                              </option>

                              <option value="fab fa-linkedin, #0D76A8">
                                Linkedin
                              </option>

                            </select>
    
                          </div>
                        </div>

                        {{--fin de blque de 5 columnas redes sociales--}}

                        <div class="col-5">
                          <div class="input-group mb-3">
                            
                            <div class="input-group-append">
                              <span class="input-group-text">Url</span>
                            </div>
                            
                            <input type="text" class="form-control" id="url_red" >

                          </div>
                        </div>

                        {{--fin de bloque de 5 columnas de URL de la red social--}}


                        {{--boton para agregar la red social --}}
                        <div class="col-2">
                           <button type="button" class="btn btn-primary w-100 agregarRed">
                             Agregar
                           </button>                           
                        </div>

                        {{--fin col 2 --}}

                      </div>
                      {{--fin del row--}}

                      <div class="row">
                        
                         @php
                          $redes = json_decode($element->redes_sociales, true); //convierto el string de la bd en array, asi ya puedo tomar cada uno de sus indices 0,1,2 etc.....
                          //echo '<pre>'; print_r($redes); echo '</pre>';

                          foreach ($redes as $key => $value){

                            echo '  <div class="col-lg-12">
                              
                                      <div class="input-group mb-3">

                                        <div class="input-group-prepend"> 
                                          <div class="input-group-text text-white" style="background:'.$value["background"].'">

                                            <i class="'.$value["icono"].'" ></i>

                                          </div>
                                        </div>

                                        <input type="text" class="form-control" value="'.$value["url"].'">

                                        <div class="input-group-prepend"> 
                                          <div class="input-group-text" style="cursor:pointer">

                                            <span class="bg-danger px-2 rounded-circle">&times;</span>

                                          </div>
                                        </div>

                                      </div>

                                    </div>  '; 
                          }
                          
                          
                          @endphp

                      </div>

                    
                    </div>
                  </div>  
                </div>
                
                <div class="col-lg-5">

                  <div class="card">

                    <div class="card-body">
                      

                      <div class="row"> {{--boton de adjuntar--}}

                        <div class="col-lg-12">

                          {{--Cambiar Logo--}}
                          <div class="form-group my-2 text-center">

                            <div class="btn btn-default btn-file mb-3">

                              <i class="fas fa-paperclip"></i>Adjuntar Imagen del Logo

                              <input type="file" name="logo">

                            </div>

                            <img src="{{url('/')}}/{{$element->logo}}" class="img-fluid py-2 bg-secondary">

                            <p class="help-block small mt-3">Dimensiones: 700px * 200px | peso Max. 2MB | formato: JPG o PNG</p>

                          </div>    

                          
                          <hr class="pb-2">

                          
                          {{--Cambiar Portada--}}
                          <div class="form-group my-2 text-center">

                            <div class="btn btn-default btn-file mb-3">

                              <i class="fas fa-paperclip"></i>Adjuntar Imagen de Portada

                              <input type="file" name="portada">

                            </div>

                            <img src="{{url('/')}}/{{$element->portada}}" class="img-fluid py-2 bg-secondary">

                            <p class="help-block small mt-3">Dimensiones: 700px * 420px | peso Max. 2MB | formato: JPG o PNG</p>

                          </div> 

                          <hr class="pb-2">

                          
                          {{--Cambiar Icono--}}
                          <div class="form-group my-2 text-center">

                            <div class="btn btn-default btn-file mb-3">

                              <i class="fas fa-paperclip"></i>Adjuntar Imagen de Icono

                              <input type="file" name="icono">

                            </div>

                            <br>
                              
                            <img src="{{url('/')}}/{{$element->icono}}" class="img-fluid py-2  rounded-circle">

                            <p class="help-block small mt-3">Dimensiones: 150px * 150px | peso Max. 2MB | formato: JPG o PNG</p>

                          </div> 

                        </div>

                      </div> {{--cierra la clase row --}}

                    </div>
                  </div>
                </div>

                {{-- 1er bloque sobre mi bloque de card dentro de una misma fila este bloque forma la celda--}}
                <div class="col-lg-6">
                  
                  <div class="card">

                    <div class="card-body">

                      <label>Sobre mi <span class="small">(Intro)</span></label>

                      <textarea class="form-control summernote" name="sobre_mi"  rows="10">{{$element->sobre_mi}}</textarea>

                    </div>

                  </div>

                </div>

                {{--2do bloque sobre mi completo--}}
                 <div class="col-lg-6">
                  
                  <div class="card">

                    <div class="card-body">

                      <label>Sobre mi <span class="small">(Completo)</span></label>

                      <textarea class="form-control summernote" name="sobre_mi_completo" rows="10">{{$element->sobre_mi_completo}}</textarea>


                    </div>

                  </div>

                </div>


              </div>
             
            </div>

            <!-- /.card-body -->
            <div class="card-footer">
                <button  type="submit" class="btn btn-primary float-right">Guardar Cambios</button>

            </div>

            <!-- /.card-footer-->
          </div>

          <!-- /.card -->
          
          </form>

        </div>

      </div>

    </div>

  </section>

  <!-- /.content -->
</div>

@endsection
{{--Siempre se debe cerrar la etiqueta de @section con @endsection--}}