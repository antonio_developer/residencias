@extends('plantilla') {{--hacemos mension a la plantilla principal que es donde esta el @yield()--}}

@section('content'){{--encapsulamos toda esta vista para convertir su contenido en dinamico y pueda ser embebida por el @yield que esta en nuestra plantilla --}}

<div class="content-wrapper" style="min-height: 184px;">

  <!-- Content Header (Page header) -->
  <section class="content-header">

    <div class="container-fluid">

      <div class="row mb-2">

        <div class="col-sm-6">

          <h1>Administradores</h1>
        </div>
        <div class="col-sm-6">

          <ol class="breadcrumb float-sm-right">

            <li class="breadcrumb-item"><a href="{{url('/')}}">Inicio</a></li>

            <!--<li class="breadcrumb-item"><a href="#">Layout</a></li>-->

            <li class="breadcrumb-item active">Blog</li>

          </ol>

        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">

    <div class="container-fluid">

      <div class="row">

        <div class="col-12">

          <!-- Default box -->
          <div class="card">

            <div class="card-header">

              <h3 class="card-title">Title</h3>

              <div class="card-tools">

                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">

                    <i class="fas fa-minus"></i></button>

                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>

              </div>

            </div>

            <div class="card-body">
                
                <!---->
                                
                <h1>Vue Grid Layout Example 4 - Drag allow/ignore elements</h1>

                <a href="https://github.com/jbaysolutions/vue-grid-layout">View project on Github</a>
                <br/>
                <a href="03-multiple-grids.html">Previous example: Multiple grids</a>
                <br/>
                <a href="05-mirrored.html">Next example: Mirrored grid layout</a>


                <div id="app" style="width: 100%;">
                    <!--<pre>{{ $data | json }}</pre>-->
                    <div>
                        <br/>
                        Ignore drag on certain elements and allow on on others.
                        <br/>
                        Click and drag the dots on the corner of each item to reposition
                        <div class="layoutJSON">
                            Displayed as <code>[x, y, w, h]</code>:
                            <div class="columns">
                                <div class="layoutItem" v-for="item in layout">
                                    <b>{{item.i}}</b>: [{{item.x}}, {{item.y}}, {{item.w}}, {{item.h}}]
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="content">
                        <!--<button @click="addItem">Add an item</button>-->
                        <grid-layout :layout="layout"
                                     :col-num="12"
                                     :row-height="30"
                                     :is-draggable="true"
                                     :is-resizable="true"
                                     :vertical-compact="true"
                                     :use-css-transforms="true"
                        >
                            <grid-item v-for="item in layout"
                                       :x="item.x"
                                       :y="item.y"
                                       :w="item.w"
                                       :h="item.h"
                                       :i="item.i"
                                       drag-allow-from=".vue-draggable-handle"
                                       drag-ignore-from=".no-drag"
                            >
                                <div class="text">
                                    <div class="vue-draggable-handle"></div>
                                    <div class="no-drag">
                                        <span>{{item.i}}</span>
                                        <br/>
                                        <button>test</button>
                                    </div>
                                </div>
                            </grid-item>
                        </grid-layout>
                    </div>

                </div>

                <script src="vue.min.js"></script>
                <script src="../dist/vue-grid-layout.umd.min.js"></script>
                <script type="text/javascript">
                    new Vue({
                        el: '#app',
                        data: {
                            layout: [
                                {"x":0,"y":0,"w":2,"h":2,"i":"0"},
                                {"x":2,"y":0,"w":2,"h":4,"i":"1"},
                                {"x":4,"y":0,"w":2,"h":5,"i":"2"},
                                {"x":6,"y":0,"w":2,"h":3,"i":"3"},
                                {"x":8,"y":0,"w":2,"h":3,"i":"4"},
                                {"x":10,"y":0,"w":2,"h":3,"i":"5"},
                                {"x":0,"y":5,"w":2,"h":5,"i":"6"},
                                {"x":2,"y":5,"w":2,"h":5,"i":"7"},
                                {"x":4,"y":5,"w":2,"h":5,"i":"8"},
                                {"x":6,"y":4,"w":2,"h":4,"i":"9"},
                                {"x":8,"y":4,"w":2,"h":4,"i":"10"},
                                {"x":10,"y":4,"w":2,"h":4,"i":"11"},
                                {"x":0,"y":10,"w":2,"h":5,"i":"12"},
                                {"x":2,"y":10,"w":2,"h":5,"i":"13"},
                                {"x":4,"y":8,"w":2,"h":4,"i":"14"},
                                {"x":6,"y":8,"w":2,"h":4,"i":"15"},
                                {"x":8,"y":10,"w":2,"h":5,"i":"16"},
                                {"x":10,"y":4,"w":2,"h":2,"i":"17"},
                                {"x":0,"y":9,"w":2,"h":3,"i":"18"},
                                {"x":2,"y":6,"w":2,"h":2,"i":"19"}
                            ],
                            index: 0
                        },
                    });



                </script>
                <!---->




            </div>

            <!-- /.card-body -->
            <div class="card-footer">
              Footer
            </div>

            <!-- /.card-footer-->
          </div>

          <!-- /.card -->
        </div>

      </div>

    </div>

  </section>

  <!-- /.content -->
</div>

@endsection
{{--Siempre se debe cerrar la etiqueta de @section con @endsection--}}