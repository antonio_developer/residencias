<!DOCTYPE html>
<html>
<head>
	
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>cms del blog</title>
	
	{{-- Tell the browser to be responsive to screen width --}}
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!--=====================================
		PLUGINS DE CSS
	======================================-->
	

	{{--Bootstrap 4 CDN--}}
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	{{--plantilla AdminLTE--}}
	<link rel="stylesheet"  href="{{ url('/') }}/css/plugins/OverlayScrollbars.min.css">

	
	{{--TAGS INPUT--}}
	<link rel="stylesheet"  href="{{ url('/') }}/css/plugins/tagsinput.css">

	{{-- SUMMERNOTE --}}
	<link rel="stylesheet"  href="{{ url('/') }}/css/plugins/summernote.css">
	{{--<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">--}}

	


	{{--plantilla AdminLTE--}}
	<link rel="stylesheet"  href="{{ url('/') }}/css/plugins/adminlte.min.css">
	{{-- donde {{ url('/') }} *sintaxis blade es = localhost/blog-php/cms/public  --}}

	{{--google fonts que son las fuentes que utiliza AdminLTE--}}
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


	<!--=====================================
		PLUGINS DE JS
	======================================-->
	

	<!--FontAwesome-->
	<script src="https://kit.fontawesome.com/e632f1f723.js" crossorigin="anonymous"></script>

		<!-- Jquery library --> 
			<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<!--<script src="https;//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
	<!--esta tenia
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
-->



	
	<!-- Pooper JS-->
	<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/pooper.min.js"></script>-->

	<!--esta version de popper.js de abajo *sig. linea me dio problemas para el uso de summernote se corrigio actualizando el CDN de la version de popper mas reciente-->
	<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/popper.min.js"></script>-->

		<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>

	<!--version de desarrollo esta version me dio problemas y no trabajaba correctamente el plugin summernote-->
	<!--<script src="https://unpkg.com/popperjs/core@2/dist/umd/popper.js"></script>-->

	<!--version de producccion-->
	<!--<script src="https://unpkg.com/popperjs/core@2"></script>-->


	<!--Latest compiled JavaScript-->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<!--jquery.overlayScrollbars.min.js -->
	<script src="{{ url('/') }}/js/plugins/jquery.overlayScrollbars.min.js"></script>


	<!--TAGS INPUT -->
	{{--https://www.jqueryscript.net/form/Bootstrap-4-Tag-Input-Plugin-jQuery.html--}} 
	<script src="{{ url('/') }}/js/plugins/tagsinput.js"></script>


	<!-- SUMMERNOTE  -->
	{{--https://summernote.org/--}} 
	<script src="{{ url('/') }}/js/plugins/summernote.js"></script>
	{{--<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>--}}
	




	<!-- JS AdminLTE --> 
	<script src="{{ url('/') }}/js/plugins/adminlte.js"></script>

	
	{{--que vamos a integrar en esta plantilla : 1  hojas de estilo 2  plugins de javascript 3 maketacion que tre adminLTE--}}


	<link rel="stylesheet" href="vue-grid-layout-master/app.css">

</head>
<body class="hold-transition sidebar-mini layout-fixed">

	<div class="wrapper">
		
		{{--nota: los modulos son fijos las paginas es por donde vamos a navegar--}}

		@include('modulos.header') {{--llamamos de la carpeta modulos el archivo header--}}

		@include('modulos.sidebar')
		
		@yield('content') {{-- El yield content \ya no me trae una sola pagina sino me trae un contenido dinamico para esto en lugar de usar @include(modulo.nombre_modulo) usamos @yield(content)--}}

		@include('modulos.footer')

	</div>

<script src="{{ url('/') }}/js/codigo.js"></script>

</body>

</html>